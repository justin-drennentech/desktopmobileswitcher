﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DesktopMobileSwitcher.Models;

namespace DesktopMobileSwitcher.Controllers {
    public class DVDController : Controller {
        private DVDContext db = new DVDContext();

        //
        // GET: /DVD/
        public ActionResult Index() {
            return View(db.DVDs.ToList());
        }

        //
        // GET: /DVD/Details/5
        public ActionResult Details(int id = 0) {
            DVD dvd = db.DVDs.Find(id);
            if (dvd == null) {
                return HttpNotFound();
            }
            return View(dvd);
        }

        //
        // GET: /DVD/Create
        public ActionResult Create() {
            return View();
        }

        //
        // POST: /DVD/Create
        [HttpPost]
        public ActionResult Create(DVD dvd) {
            if (ModelState.IsValid) {
                db.DVDs.Add(dvd);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(dvd);
        }

        //
        // GET: /DVD/Edit/5
        public ActionResult Edit(int id = 0) {
            DVD dvd = db.DVDs.Find(id);
            if (dvd == null) {
                return HttpNotFound();
            }
            return View(dvd);
        }

        //
        // POST: /DVD/Edit/5
        [HttpPost]
        public ActionResult Edit(DVD dvd) {
            if (ModelState.IsValid) {
                db.Entry(dvd).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dvd);
        }

        //
        // GET: /DVD/Delete/5
        public ActionResult Delete(int id = 0) {
            DVD dvd = db.DVDs.Find(id);
            if (dvd == null) {
                return HttpNotFound();
            }
            return View(dvd);
        }

        //
        // POST: /DVD/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id) {
            DVD dvd = db.DVDs.Find(id);
            db.DVDs.Remove(dvd);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}