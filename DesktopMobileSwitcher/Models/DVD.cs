﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesktopMobileSwitcher.Models {
    public class DVD {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public Rating Rating { get; set; }
    }

    public enum Rating {
        G,
        PG,
        PG13,
        R
    }
}